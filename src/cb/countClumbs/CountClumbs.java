package cb.countClumbs;

import java.util.HashSet;
import java.util.Set;
/*
 * @see https://codingbat.com/prob/p193817
 */
public class CountClumbs {

	public int countClumbs(int[] nums) {
		int count = 0, i = 0;
		if (nums.length == 0) {
			return 0;
		}
		if (arrayHasSameNumber(nums)) {
			return 1;
		}
		while (i < nums.length) {
			if (nums[i] == 2) {
				if (hasAdjacent(i, nums)) {
					count++;
					i++;
				}
			} else {
				if (hasAdjacent(i, nums)) {
					count++;
					i++;
				}
			}
			i++;
		}

		return count;
	}

	public boolean hasAdjacent(int index, int[] nums) {
		if (index + 1 < nums.length) {
			return nums[index] == nums[index + 1];
		}
		return false;
	}

	public boolean arrayHasSameNumber(int[] nums) {
		Set<Integer> set = new HashSet<>();
		for (int val : nums) {
			set.add(val);
		}
		return set.size() == 1;
	}
}
